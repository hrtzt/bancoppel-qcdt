// Vendor
// @prepros-prepend ../vendor/browserselector/browserselector.js
// @prepros-prepend ../vendor/jquery/jquery-3.4.1.min.js
// @prepros-prepend ../vendor/wpkitui/wpkitui.js
// @prepros-prepend ../vendor/fancybox/fancybox.js
// @prepros-prepend ../vendor/slick/slick.js
// prepros-prepend ../vendor/tweenmax/TweenMax.min.js
// prepros-prepend ../vendor/tweenmax/TweenMax.min.js


// Components

// @prepros-prepend ../../components/head/head.js
// @prepros-prepend ../../components/header/header.js
// @prepros-prepend ../../components/footer/footer.js
// @prepros-prepend ../../components/sidebar/sidebar.js
// @prepros-prepend ../../components/offcanvas/offcanvas.js

// Modules

// @prepros-prepend ../../modules/login/login.js
// @prepros-prepend ../../modules/contacto/contacto.js
// @prepros-prepend ../../modules/aviso-de-privacidad/aviso-de-privacidad.js
// @prepros-prepend ../../modules/bases/bases.js
// @prepros-prepend ../../modules/loged/loged.js
// @prepros-prepend ../../modules/participa/participa.js
// @prepros-prepend ../../modules/perfil/perfil.js
// @prepros-prepend ../../modules/registro/registro.js
// @prepros-prepend ../../modules/terminos-y-condiciones/terminos-y-condiciones.js
// @prepros-prepend ../../modules/recuperacion-de-contrasena/recuperacion-de-contrasena.js
// @prepros-prepend ../../modules/cuenta-vinculada/cuenta-vinculada.js
// @prepros-prepend ../../modules/diseno-cargado/diseno-cargado.js
// @prepros-prepend ../../modules/votacion/votacion.js
// @prepros-prepend ../../modules/tarjeta/tarjeta.js

